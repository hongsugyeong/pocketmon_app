class MonsterItem{
  num id;
  String imgUrl;
  String charName;
  String classification;
  num vitality;
  num power;
  num defensive;
  num speed;

  MonsterItem(this.id, this.imgUrl, this.charName, this.classification, this.vitality, this.power, this.defensive, this.speed);
}