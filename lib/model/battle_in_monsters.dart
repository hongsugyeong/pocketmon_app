import 'package:pocketmon_app/model/monster_battle.dart';

class BattleInMonsters {
  MonsterBattle monster1;
  MonsterBattle monster2;

  BattleInMonsters(this.monster1, this.monster2);

  factory BattleInMonsters.fromJson(Map<String, dynamic> json) {
    return BattleInMonsters (
      MonsterBattle.fromJson(json['monster1']),
      MonsterBattle.fromJson(json['monster2']),
    );
  }
}