class MonsterBattle {
  num monsterBattleId;
  num monsterId;
  String imgUrl;
  String monsterName;
  num monsterVitality;
  num monsterPower;
  num monsterDefensive;
  num monsterSpeed;

  MonsterBattle(
      this.monsterBattleId,
      this.monsterId,
      this.imgUrl,
      this.monsterName,
      this.monsterVitality,
      this.monsterPower,
      this.monsterDefensive,
      this.monsterSpeed,
      );

  factory MonsterBattle.fromJson(Map<String, dynamic> json) {
    return MonsterBattle(
      json['monsterBattleId'],
      json['monsterId'],
      json['imgUrl'],
      json['monsterName'],
      json['monsterVitality'],
      json['monsterPower'],
      json['monsterDefensive'],
      json['monsterSpeed'],
    );
  }
}