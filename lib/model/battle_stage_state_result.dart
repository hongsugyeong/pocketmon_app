import 'package:pocketmon_app/model/battle_in_monsters.dart';

class BattleStageStateResult {
  BattleInMonsters data;

  BattleStageStateResult(this.data);

  factory BattleStageStateResult.fromJson(Map<String, dynamic> json) {
    return BattleStageStateResult (
      BattleInMonsters.fromJson(json['data']),
    );
  }
}