import 'package:dio/dio.dart';
import 'package:pocketmon_app/config/config_api.dart';
import 'package:pocketmon_app/model/battle_stage_state_result.dart';
import 'package:pocketmon_app/model/common_result.dart';

class RepoBattleStage {
  Future<BattleStageStateResult> getCurrentState() async {
    Dio dio = Dio(); // 꼭 먼저 패키지 추가하기
    String _baseUrl = '$apiUri/battle/current/state'; // config 있어야 가능

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );

    return BattleStageStateResult.fromJson(response.data);
  }

  Future<CommonResult> setBattleIn(num monsterId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/battle/stage/in/monster-id/{monsterId}';

    final response = await dio.post(
      _baseUrl.replaceAll('{monsterId}', monsterId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> delBattleMonster(num battleId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/battle/stage/out/stage-id/{stageId}';

    final response = await dio.delete(
      _baseUrl.replaceAll('{stageId}', battleId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );

    return CommonResult.fromJson(response.data);
  }
}