import 'package:flutter/material.dart';
import 'package:pocketmon_app/model/monster_battle.dart';
import 'package:pocketmon_app/pages/page_battle_detail.dart';
import 'package:pocketmon_app/repository/repo_battle_stage.dart';

class PageBattleStage extends StatefulWidget {
  const PageBattleStage({super.key});

  @override
  State<PageBattleStage> createState() => _PageBattleStageState();
}

class _PageBattleStageState extends State<PageBattleStage> {
  MonsterBattle? monster1;
  MonsterBattle? monster2;

  // 현재 배틀장에 진입중인 몬스터들 정보 가져오기
  Future<void> _loadDetail() async {
    await RepoBattleStage().getCurrentState()
        .then((res) => {
          setState(() {
            monster1 = res.data.monster1;
            monster2 = res.data.monster2;
          })
    });
  }

  // 배틀장에서 몬스터 퇴장시키기
  Future<void> _delMonster(num stageId) async {
    await RepoBattleStage().delBattleMonster(stageId)
        .then((res) => {
          _loadDetail() // 퇴장 성공시 현재 배틀장에 진입중인 몬스터 정보 새로 가져와서 다시 갱신하기
    });
  }

  // 페이지 생성된 후 화면에 부착하기 전에 데이터 줘야
  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('배틀장 대기실'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          monster1 != null ?
              Container(
                child: Column(
                  children: [
                    Text(monster1!.monsterName),
                    OutlinedButton(
                        onPressed: () {
                          _delMonster(monster1!.monsterBattleId);
                        },
                        child: Text('퇴장')
                    ),
                  ],
                ),
              ) : Container(),

          monster2 != null ?
              Container(
                child: Column(
                  children: [
                    Text(monster2!.monsterName),
                    OutlinedButton(
                        onPressed: () {
                          _delMonster(monster2!.monsterBattleId);
                        },
                        child: Text('퇴장'),
                    )
                  ],
                ),
              ) : Container(),

          monster1 != null && monster2 != null ?
              OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) =>
                            PageBattleDetail(
                                monster1: monster1!,
                                monster2: monster2!
                            )));
                  },
                  child: Text('배틀'),
              ) : Container(),
        ],
      ),
    );
  }
}
