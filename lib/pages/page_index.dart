import 'package:flutter/material.dart';
import 'package:pocketmon_app/components/component_monster_item.dart';
import 'package:pocketmon_app/model/monster_item.dart';
import 'package:pocketmon_app/pages/page_monster_detail.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({
    super.key,
  });


  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  List<MonsterItem> _item = [
    MonsterItem(1, 'assets/po1.png', '이상해씨', '씨앗포켓몬', 250, 49, 49, 45),
    MonsterItem(2, 'assets/po2.png', '파이리', '도룡뇽포켓몬', 250, 49, 49, 45),
    MonsterItem(3, 'assets/po3.png', '꼬부기', '꼬마거북포켓몬', 250, 49, 49, 45),
    MonsterItem(4, 'assets/po4.png', '꼬렛', '씨앗포켓몬', 250, 49, 49, 45),
    MonsterItem(5, 'assets/po5.png', '피캬츄', '쥐포켓몬', 250, 49, 49, 45),
    MonsterItem(6, 'assets/po6.png', '니드런', '독침포켓몬', 250, 49, 49, 45),
    MonsterItem(7, 'assets/po7.png', '삐삐', '요정포켓몬', 250, 49, 49, 45),
    MonsterItem(8, 'assets/po8.png', '푸린', '풍선포켓몬', 250, 49, 49, 45),
    MonsterItem(9, 'assets/po9.png', '나옹', '요괴고양이포켓몬', 250, 49, 49, 45),
    MonsterItem(10, 'assets/po10.png', '고라파덕', '오리포켓몬', 250, 49, 49, 45),
    MonsterItem(11, 'assets/po11.png', '마그비', '불씨포켓몬', 250, 49, 49, 45),
    MonsterItem(12, 'assets/po12.png', '애버라스', '바위표면포켓몬', 250, 49, 49, 45),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            '포켓몽스터',
          ),
        ),
        body: _buildBody(context));
  }

  Widget _buildBody(BuildContext context) {
    return GridView.builder(
        itemCount: _item.length,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: 1 / 2,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10),
        itemBuilder: (BuildContext ctx, int idx) {
          return Container(
            child: Column(
                  children: [
                    ComponentMonsterItem(
                      monsterItem: _item[idx],
                      callback: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                PageMonsterDetail(
                                    monsterItem: _item[idx],
                                    callback: () {},
                                )));
                      },
                    ),
                  ],
                ),
          );
        });
  }
}
