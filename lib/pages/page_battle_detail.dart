import 'package:flutter/material.dart';
import 'package:pocketmon_app/components/component_monster_battle.dart';
import 'package:pocketmon_app/model/monster_battle.dart';

class PageBattleDetail extends StatefulWidget {
  const PageBattleDetail({
    super.key,
    required this.monster1,
    required this.monster2,
  });

  final MonsterBattle monster1;
  final MonsterBattle monster2;

  @override
  State<PageBattleDetail> createState() => _PageBattleDetailState();
}

class _PageBattleDetailState extends State<PageBattleDetail> {

  // 기본값: 1번 몬스터 먼저 공격
  bool _isTurnMonster1 = true;

  // 1번 전용: 배틀할 때 살았는지, 현재 남은 HP 구하기
  bool _monster1Live = true;
  num _monster1CurrentHp = 0;

  // 2번 전용: 배틀할 때 살았는지, 현재 남은 HP 구하기
  bool _monster2Live = true;
  num _monster2CurrentHp = 0;

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn(); // 몬스터 선공격 판별
    setState(() {
      // 배틀장에 입장했을 때이기 때문에 HP 만땅
      _monster1CurrentHp = widget.monster1.monsterVitality;
      _monster2CurrentHp = widget.monster2.monsterVitality;
    });
  }

  // 기본값은 1번 몬스터가 선공격이지만 2번 몬스터가 선공격할 경우 고려
  void _calculateFirstTurn() {
    if (widget.monster1.monsterSpeed < widget.monster2.monsterSpeed) {
      setState(() {
        _isTurnMonster1 = false;
      });
    }
  }

  num _calculateResultHitPoint(num myHitPower, num targetDefPower) {
    List<num> criticalArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]; // 10%의 확률
    criticalArr.shuffle(); // 갑자기 왜 섞음?

    bool isCritical = false;
    if (criticalArr[0] == 1) isCritical = true;

    num resultHit = myHitPower; // 공격력 resultHit으로 선언해주기
    if (isCritical) resultHit = resultHit*4; // 만약 크리티컬이 대체 뭔데
    resultHit = resultHit - targetDefPower; // 상대방 방어력만큼 데미지 차감하기
    resultHit = resultHit.round(); // 반올림 처리 또는 정수 처리

    if (resultHit <= 0) resultHit = 1; // 계산결과 공격력이 0이거나 음수일 경우 공격력 1로 고정 처리

    return resultHit; // 결과값 반환
  }

  // 상대 몬스터 죽었는지 판별하기
  void _checkIsDead(num targetMonster) {
    if (targetMonster == 1 && _monster1CurrentHp <= 0) { // 타겟 몬스터가 1이고, 현재 HP가 0 이하일 경우
      setState(() {
        _monster1Live = false; // 1번 몬스터 죽음 ㅅㄱ
      });
    } else if (targetMonster == 2 && _monster2CurrentHp <= 0) { // 타겟 몬스터가 2이고, 현재 HP가 0 이하일 경우
      setState(() {
        _monster2Live = false; // 2번 몬스터도 갔슈
      });
    }
  }

  // 공격 처리하기
  void _attMonster (num actionMonster) {
    num myHisPower = widget.monster1.monsterPower; // 몬스터 1의 공격력
    num targetDefPower = widget.monster2.monsterDefensive; // 몬스터 2의 방어력

    // 위의 초기 설정이랑 반대로 선언하기
    // 배틀장에 투입된 몬스터가 2명일 경우 선언?
    if (actionMonster == 2) {
      myHisPower = widget.monster2.monsterPower;
      targetDefPower = widget.monster1.monsterDefensive;
    }

    num resultHit = _calculateResultHitPoint(myHisPower, targetDefPower); // 상대 HP 차감 계산하기

    if (actionMonster == 1) { // 공격하는 몬스터가 1번일 경우, 당하는 몬스터는 2번
      setState(() { // 새로 화면 만들어주기
        _monster2CurrentHp -= resultHit; // 2번 몬스터 체력 차감 (2번 몬스터의 현재 HP에서 상대 공격력만큼 차감하기)
        if (_monster2CurrentHp <= 0) _monster2CurrentHp = 0; // 2번 몬스터의 체력이 0 미만이면 0으로 처리 (체력 0되면 게임 끝나기 때문에)
          _checkIsDead(2); // 2번 몬스터 죽었는지 확인하기 (위의 판별식에 대입하기)
      });
    } else { // 공격하는 몬스터가 2번일 경우, 당하는 몬스터는 1번
      setState(() { // 새로 화면 만들어주기
        _monster1CurrentHp -= resultHit; // 1번 몬스터 체력 차감 (1번 몬스터의 현재 HP에서 상대 공격력만큼 차감하기)
        if (_monster1CurrentHp <= 0) _monster1CurrentHp = 0; // 1번 몬스터의 체력이 0 미만이면 0으로 처리 (체력이 0이 되는 순간 게임 끝)
        _checkIsDead(1); // 위의 판별식에 대입하여 1번 몬스터 죽었는지 확인하기
      });
    }

    // 아무도 죽지 않고 베틀이 계속해서 진행될 경우 순서 넘기기
    setState(() {
      _isTurnMonster1 = !_isTurnMonster1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('포켓몽스터 대결 화면'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          children: [
            ComponentMonsterBattle(
                callback: () {
                  _attMonster(1);
                },
                monsterBattle: widget.monster1,
                isMyTurn: _isTurnMonster1,
                isLive: _monster1Live,
                currentHp: _monster1CurrentHp,
            ),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('VS')
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            ComponentMonsterBattle(
                callback: () {
                  _attMonster(2);
                },
                monsterBattle: widget.monster2,
                isMyTurn: !_isTurnMonster1,
                isLive: _monster2Live,
                currentHp: _monster2CurrentHp,
            ),
          ],
        ),
      ],
    );
  }
}
