import 'package:flutter/material.dart';
import 'package:pocketmon_app/model/monster_battle.dart';
import 'package:pocketmon_app/model/monster_item.dart';
import 'package:pocketmon_app/pages/page_battle_detail.dart';
import 'package:pocketmon_app/pages/page_battle_stage.dart';

class PageMonsterDetail extends StatefulWidget {
  PageMonsterDetail({
    super.key,
    required this.monsterItem,
    required this.callback,
  });

  final MonsterItem monsterItem;
  final VoidCallback callback;

  @override
  State<PageMonsterDetail> createState() => _PageMonsterDetailState();
}

class _PageMonsterDetailState extends State<PageMonsterDetail> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("포켓몬 상세보기"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(widget.monsterItem.imgUrl),
            Text(widget.monsterItem.charName),
            Text(widget.monsterItem.classification),
            Text('총 HP: ${widget.monsterItem.vitality}'),
            Text('공격력: ${widget.monsterItem.power}'),
            Text('방어력: ${widget.monsterItem.defensive}'),
            Text('스피드: ${widget.monsterItem.speed}'),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) =>
                        PageBattleStage()));
              },
              child: Text('대기실 입장하기'),
            )
          ],
        ),
      ),
    );
  }
}

