import 'package:flutter/material.dart';
import 'package:pocketmon_app/model/monster_item.dart';

class ComponentMonsterItem extends StatelessWidget {
  const ComponentMonsterItem({
    super.key,
    required this.monsterItem,
    required this.callback
  });

  final MonsterItem monsterItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Image.asset(
            monsterItem.imgUrl
          ),
          Column(
            children: [
              Text(
                  '${monsterItem.id}'
              ),
              Text(
                monsterItem.charName
              ),
              Text(
                monsterItem.classification
              ),
            ],
          )
        ],
      ),
    );
  }
}
