import 'package:flutter/material.dart';

import '../model/monster_battle.dart';

class ComponentMonsterBattle extends StatefulWidget {
  const ComponentMonsterBattle({
    super.key,
    required this.callback,
    required this.monsterBattle,
    required this.isMyTurn,
    required this.isLive,
    required this.currentHp,
  });

  final VoidCallback callback;
  final MonsterBattle monsterBattle;
  final bool isMyTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentMonsterBattle> createState() => _ComponentMonsterBattleState();
}

class _ComponentMonsterBattleState extends State<ComponentMonsterBattle> {
  double _calculateHpPercent () {
    return widget.currentHp / widget.monsterBattle.monsterVitality;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.callback,
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width/ 4,
            child: LinearProgressIndicator(
              value: _calculateHpPercent(),
              backgroundColor: Colors.black26,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
          SizedBox(
            child: Image.asset(
              '${widget.isLive ? widget.monsterBattle.imgUrl : 'assets/death.png'}',
              width: 300,
              height: 300,
            ),
          ),
          Text(widget.monsterBattle.monsterName),
          Text('총 HP: ${widget.monsterBattle.monsterVitality}'),
          Text('공격력: ${widget.monsterBattle.monsterPower}'),
          Text('방어력: ${widget.monsterBattle.monsterDefensive}'),
          Text('스피드: ${widget.monsterBattle.monsterSpeed}'),
          (widget.isMyTurn && widget.isLive) ? // 조건식 제시
          // 실행식
          ElevatedButton(
              onPressed: widget.callback,
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  foregroundColor: Colors.white,
                  shadowColor: Colors.black26,
                  elevation: 10.0,
                  textStyle: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 20.0,
                  )
              ),
              child: Text(
                '공격하기',
                textAlign: TextAlign.center,
              )
          ) : Container(), // 반복문처럼 값 집어넣기
        ],
      ),
    );
  }
}
